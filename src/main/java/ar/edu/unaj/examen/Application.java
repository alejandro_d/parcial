package ar.edu.unaj.examen;

import ar.edu.unaj.examen.component.InitData;
import ar.edu.unaj.examen.component.Operation;
import ar.edu.unaj.examen.frame.AppPrincipalFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;

import javax.swing.*;

@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {

        new SpringApplicationBuilder(Application.class)
                .headless(false)
                .web(false)
                .run(args);
    }
    @Autowired
    Operation operation;

    @Bean
    public AppPrincipalFrame frame() {
        return new AppPrincipalFrame(operation);
    }

    @Bean
    public InitData init(){
        return new InitData();
    }

}
