package ar.edu.unaj.examen.utils;

import ar.edu.unaj.examen.config.DbConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Component
public class ExternalCallUtility {

    @Autowired
    DbConnection dbConnection;

    public String call(int documentNumber, String typeDocument, int transaction){
        String status = "ok";
        String call = "{call spGetStatusFromDocAndTrx(?,?,?)}";
        try (CallableStatement stmt = dbConnection.getConnection().prepareCall(call)) {
            stmt.setInt(1, Integer.valueOf(documentNumber));
            stmt.setString(2, typeDocument);
            stmt.setInt(3, Integer.valueOf(transaction));

            ResultSet rs = stmt.executeQuery();
            if(rs.next()){
                status = rs.getString(0);
            }
        }catch (Exception e){
            //todo handler exception
        }
        return status;
    }

    public void createStore () throws Exception {
        String sqlString = " CREATE PROCEDURE spGetStatusFromDocAndTrx (IN doc_number int, IN doc_type varchar(3), IN trx int , OUT status varchar(50))  ";
                sqlString += " BEGIN ";
                sqlString += "   SELECT 'OK';" ;
                sqlString += " END;";
        try {
            Statement statement = dbConnection.getConnection().createStatement();

            //statement.execute(sqlString);
        }finally {
            dbConnection.getConnection().close();
        }
    }

}
