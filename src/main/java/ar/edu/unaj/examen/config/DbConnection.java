package ar.edu.unaj.examen.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Component
public class DbConnection {

    private Connection connection;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.driver-class-name}")
    private String driverData;

    @Value("${spring.datasource.username}")
    private String user;

    @Value("${spring.datasource.password}")
    private String pass;

    public Connection getConnection() throws Exception {
        if(connection==null){
            Class.forName(driverData);
            connection = DriverManager.getConnection(url,user,pass);
        }
        return connection;
    }


    public CallableStatement prepareCall(String call) throws Exception {
        try {
            return getConnection().prepareCall(call);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  null;
    }
}
