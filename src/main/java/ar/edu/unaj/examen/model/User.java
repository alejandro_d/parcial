package ar.edu.unaj.examen.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private int id;

    @Column(name = "alias_account", unique = true, length = 50 , nullable = false)
    private String aliasAccount;

    @Column
    private String name;

    @Column
    private String surname;

    @Column(length = 150)
    private String email;

    @Column(name = "document_number")
    private int documentNumber;

    @Column(name = "document_type")
    private String documentType;

}
