package ar.edu.unaj.examen.respository;

import ar.edu.unaj.examen.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByDocumentNumberAndDocumentType(int documentNumber, String documentType);
}