package ar.edu.unaj.examen.frame;

import ar.edu.unaj.examen.component.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;

@Component
public class AppPrincipalFrame extends JFrame{


    private static final long serialVersionUID = 1L;

    final private JTextField txtDocNumber, txtDocType, txtTrxNumber;

    private JButton btnValidate;

    private JButton btnExit;

    Operation operation;

    public AppPrincipalFrame(Operation operation){

        this.operation = operation;
        txtDocNumber =new JTextField(6);
        txtDocNumber.setEnabled(true);
        txtDocType =new JTextField(6);
        txtDocType.setEnabled(true);
        txtTrxNumber =new JTextField(6);
        txtTrxNumber.setEnabled(true);

        JPanel panel=new JPanel();
        panel.add(new JLabel("Doc. Nro:"));
        panel.add(txtDocNumber);
        panel.add(new JLabel("Doc Tipo:"));
        panel.add(txtDocType);
        panel.add(new JLabel("Trx Nro"));
        panel.add(txtTrxNumber);

        btnValidate = new JButton("Comprobar");
        btnValidate.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                operation.doOperation(Integer.valueOf(txtDocNumber.getText()).intValue(),txtDocType.getText(),Integer.valueOf(txtTrxNumber.getText()).intValue());

            }

        });

        btnExit = new JButton("Salir");
        btnExit.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        panel.add(btnValidate);
        panel.add(btnExit);

        setTitle("Validador");
        setSize(620,180);

        setDefaultLookAndFeelDecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(panel);

    }

}