package ar.edu.unaj.examen.component;

import ar.edu.unaj.examen.model.User;
import ar.edu.unaj.examen.respository.UserRepository;
import ar.edu.unaj.examen.utils.EmailUtility;
import ar.edu.unaj.examen.utils.ExternalCallUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.Optional;

@Component
public class Operation {

    @Autowired
    UserRepository userRepository;

    @Autowired
    EmailUtility emailUtility;

    @Autowired
    ExternalCallUtility externalCallUtility;

    public String doOperation(int doc, String type, int trx){
        Optional<User> userOptional = userRepository.findByDocumentNumberAndDocumentType(doc,type);
        User user = null;
        String status = "";
        if(userOptional.isPresent()){
            user = userOptional.get();
        }
        if(user != null) {
            status = externalCallUtility.call(user.getDocumentNumber(), user.getDocumentType(), trx);
            try {
                emailUtility.sendEmailTo(user.getEmail(),"Estado de la consulta",getMessage(user, trx, status));
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }

        return status;
    }

    private String getMessage(User user, int trx, String status){
        String msg = "Hola " + user.getName() +"\r\n";
        msg += "El estado de la consulta nro: " + trx + " es : "+  status +"\r\n";
        msg += " Ante cualquier duda estamos a su disposición " + "\r\n";
        msg += " Slds.";
        return msg;
    }
}
