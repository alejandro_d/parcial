package ar.edu.unaj.examen.component;

import ar.edu.unaj.examen.model.User;
import ar.edu.unaj.examen.respository.UserRepository;
import ar.edu.unaj.examen.utils.ExternalCallUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class InitData implements ApplicationRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ExternalCallUtility externalCallUtility;

    static boolean startApp = false;

    public int initializer(){

        if(!startApp) {
            startApp = true;
            User user = new User();
            user.setName("Peter");
            user.setSurname("Torval");
            user.setAliasAccount("peter28");
            user.setDocumentNumber(111);
            user.setDocumentType("dc");
            user.setEmail("alevazquez.d+test1@gmail.com");
            userRepository.save(user);

            try {
                //implementarlo en otro motor
                externalCallUtility.createStore();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public void run(ApplicationArguments args) {
        initializer();
    }
}
